package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/kkyr/fig"
	"gitlab.com/3stadt/steelbot/bot"
	"gitlab.com/3stadt/steelbot/led"
)

type Config struct {
	Twitch struct {
		UserName    string `fig:"user_name" validate:"required"`
		TwitchOAuth string `fig:"twitch_oauth" validate:"required"`
		Channel     string `fig:"channel" validate:"required"`
	} `fig:"twitch"`
	Blink struct {
		NewMessage          string   `fig:"new_message_pattern" default:"newMessage"`
		HighlightMessage    string   `fig:"highlight_message_pattern" default:"highlightMessage"`
		IgnoreUsers         []string `fig:"ignore_users"`
		HighlightWords      []string `fig:"highlight_words"`
		MaxBlinkFrequencyMs int      `fig:"max_blink_frequency_ms" default:"1000"`
		Port                int      `fig:"port" default:"8934"`
		Active              bool     `fig:"active"`
	} `fig:"blink"`
	BanHammer struct {
		Active bool   `fig:"active"`
		BotApi string `fig:"bot_api" validate:"required"`
	} `fig:"ban_hammer"`
	Http struct {
		Port   int  `fig:"port" default:"8081"`
		Active bool `fig:"active"`
	} `fig:"http"`
}

var msgChan chan led.Message

func main() {
	logfile, err := os.OpenFile("steelbot.log", os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer logfile.Close()
	log.SetOutput(logfile)

	var cfg Config
	err = fig.Load(&cfg, fig.File("settings.toml"))
	if err != nil {
		log.Println(fmt.Sprintf("ERROR %s", err.Error()))
		os.Exit(1)
	}

	log.Println("STARTING steelbot")
	log.Printf("BLINKER is active: %t\n", cfg.Blink.Active)
	log.Printf("HTTP SERVER is active: %t\n", cfg.Http.Active)
	log.Printf("BANHAMMER is active: %t\n", cfg.BanHammer.Active)

	b := bot.BotConfig{
		UserName:         cfg.Twitch.UserName,
		TwitchOAuth:      cfg.Twitch.TwitchOAuth,
		Channel:          cfg.Twitch.Channel,
		OnPrivateMessage: onPrivMsg,
		BanHammer: struct {
			Active bool
			BotApi string
		}{
			cfg.BanHammer.Active,
			cfg.BanHammer.BotApi,
		},
	}

	msgChan = led.RunInBackground(
		cfg.Blink.NewMessage,
		cfg.Blink.HighlightMessage,
		cfg.Blink.IgnoreUsers,
		cfg.Blink.HighlightWords,
		cfg.Blink.MaxBlinkFrequencyMs,
		cfg.Blink.Port,
		cfg.Blink.Active,
	)
	go func() {
		err = b.Run()
		if err != nil {
			log.Println(fmt.Sprintf("ERROR %s", err.Error()))
			os.Exit(1)
		}
	}()

	log.Println("STARTED steelbot")
	if !cfg.Http.Active {
		select {} // keep running forever
		return
	}

	http.Handle("/", http.FileServer(http.Dir("./htdocs")))
	http.HandleFunc("/get", func(w http.ResponseWriter, r *http.Request) {
		file := r.URL.Query().Get("file")
		if file == "" {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("missing query parameter: file"))
			return
		}
		content, err := ioutil.ReadFile(file)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(err.Error()))
			return
		}
		w.Write(content)
	})

	err = http.ListenAndServe(fmt.Sprintf("localhost:%d", cfg.Http.Port), nil)
	if err != nil {
		log.Println(fmt.Sprintf("ERROR %s", err.Error()))
		os.Exit(1)
	}
}

func onPrivMsg(u, m string) (string, error) {
	msgChan <- led.Message{User: u, Text: m}
	return "", nil
}
