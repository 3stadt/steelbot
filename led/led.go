package led

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

var msg chan Message = make(chan Message)
var lastBlink time.Time = time.Now().Add(-time.Hour)

type Message struct {
	User string
	Text string
}

type conf struct {
	NewMessage        string
	HighlightMessage  string
	IgnoreUsers       map[string]struct{}
	HighlightWords    []string
	MaxBlinkFrequency int
	Port              int
	Active            bool
}

var maxBlinkFrequency time.Duration

func RunInBackground(newMessage, highlightMessage string, ignoreUsers, highlightWords []string, maxBlinkFreq, port int, active bool) chan Message {
	var err error
	c := conf{
		NewMessage:        newMessage,
		HighlightMessage:  highlightMessage,
		IgnoreUsers:       make(map[string]struct{}),
		HighlightWords:    highlightWords,
		MaxBlinkFrequency: maxBlinkFreq,
		Port:              port,
		Active:            active,
	}
	for _, ig := range ignoreUsers {
		c.IgnoreUsers[strings.ToLower(ig)] = struct{}{}
	}
	maxBlinkFrequency, err = time.ParseDuration(fmt.Sprintf("%dms", c.MaxBlinkFrequency))
	if err != nil {
		log.Printf("ERROR setting max blink frequency: %s\n", err.Error())
	}
	go c.listen()
	return msg
}

func (c conf) listen() {
	for {
		newMsg := <-msg
		if !c.Active || time.Since(lastBlink) < maxBlinkFrequency {
			continue
		}
		if _, exists := c.IgnoreUsers[strings.ToLower(newMsg.User)]; exists {
			continue
		}
		lastBlink = time.Now()
		_, err := http.Get(fmt.Sprintf("http://localhost:%d/blink1/pattern/play?pname=%s", c.Port, c.getPattern(newMsg.Text)))
		if err != nil {
			log.Printf("ERROR initiating blink: %s\n", err.Error())
		}
	}
}

func (c conf) getPattern(msgText string) string {
	msgText = strings.ToLower(msgText)
	for _, term := range c.HighlightWords {
		if strings.Contains(msgText, strings.ToLower(term)) {
			return c.HighlightMessage
		}
	}
	return c.NewMessage
}
