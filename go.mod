module gitlab.com/3stadt/steelbot

go 1.16

require (
	github.com/gempir/go-twitch-irc/v2 v2.5.0
	github.com/kkyr/fig v0.2.0
)
