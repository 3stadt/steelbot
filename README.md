# Steelbot

> This is a custom Twitch bot, created for the awesome streamer personality [BlackVersus](https://twitch.tv/BlackVersus).

## Binaries

- Download for Windows: [Latest binary](https://gitlab.com/3stadt/steelbot/-/jobs/artifacts/main/download?job=production)

## Documentation

No.