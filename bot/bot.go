package bot

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gempir/go-twitch-irc/v2"
)

const generatedBotFile = "botlist.txt"

type botAPIResp struct {
	Bots  [][]interface{} `json:"bots"`
	Total uint            `json:"_total"`
}

type BotConfig struct {
	UserName         string
	TwitchOAuth      string
	Channel          string
	OnPrivateMessage func(user, message string) (string, error)
	client           *twitch.Client
	BanHammer        struct {
		Active bool
		BotApi string
	}
}

func (b BotConfig) Run() error {
	b.client = twitch.NewClient(b.UserName, b.TwitchOAuth)
	
	b.client.OnPrivateMessage(func(m twitch.PrivateMessage) {
		response, err := b.OnPrivateMessage(m.User.Name, m.Message)
		if err != nil {
			log.Println(fmt.Sprintf("ERROR %s", err.Error()))
			return
		}
		if response == "" {
			return
		}
		b.client.Say(m.Channel, response)
	})

	if b.BanHammer.Active {
		b.startBanHammer()
	}

	b.client.Join(b.Channel)
	return b.client.Connect()
}

func (b BotConfig) startBanHammer() {

	bots, err := b.getBots()
	if err != nil {
		log.Println(fmt.Sprintf("ERROR could not get bot list: %s", err.Error()))
		return
	}

	b.client.OnUserJoinMessage(func(m twitch.UserJoinMessage) {
		newUser := strings.ToLower(m.User)
		log.Println(fmt.Sprintf("JOINED %s", newUser))
		if _, exists := bots[newUser]; exists {
			b.client.Say(m.Channel, fmt.Sprintf("/ban %s", newUser))
			log.Println(fmt.Sprintf("BANNED %s", newUser))
			return
		}
	})
}

func (b BotConfig) getBots() (map[string]struct{}, error) {
	bots := make(map[string]struct{})
	_, err := os.Stat(generatedBotFile)
	if os.IsNotExist(err) {
		bots, err := getBotNamesFromAPI(b.BanHammer.BotApi)
		if err != nil {
			return bots, err
		}
		file, err := os.Create(generatedBotFile)
		if err != nil {
			return bots, err
		}
		defer file.Close()
		for line, _ := range bots {
			file.WriteString(line + "\n")
		}
		return bots, err
	}

	bots, err = getBotNamesFromFile(generatedBotFile)
	return bots, err
}

func getBotNamesFromAPI(url string) (map[string]struct{}, error) {
	bots := make(map[string]struct{})

	resp, err := http.Get(url)
	if err != nil {
		return bots, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return bots, err
	}

	var bar botAPIResp

	err = json.Unmarshal(body, &bar)
	if err != nil {
		return bots, err
	}

	for _, bot := range bar.Bots {
		bots[strings.ToLower(bot[0].(string))] = struct{}{}
	}

	return bots, err
}

func getBotNamesFromFile(path string) (map[string]struct{}, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	bots := make(map[string]struct{})
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		bots[strings.ToLower(strings.TrimSpace(scanner.Text()))] = struct{}{}
	}
	return bots, scanner.Err()
}
